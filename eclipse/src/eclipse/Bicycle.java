// Neil Fisher (1939668)

package eclipse;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;

	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public int getNumberGears() {
		return numberGears;
	}

	public double getMaxSpeed() {
		return maxSpeed;
	}

	public String toString() {
		return "Manufacturer: " + getManufacturer() + ", Number of Gears: " + getNumberGears() + ", Max Speed: "
				+ getMaxSpeed();
	}
}