// Neil Fisher (1939668)

package eclipse;

public class BikeStore {
	public static void main(String[] args) {
		Bicycle[] bikes = new Bicycle[4];

		bikes[0] = new Bicycle("Raleigh", 18, 40);
		bikes[1] = new Bicycle("Focus", 21, 40);
		bikes[2] = new Bicycle("Felt", 18, 40);
		bikes[3] = new Bicycle("Trek", 16, 40);

		for (int i = 0; i < bikes.length; i++) {
			System.out.println(bikes[i]);
		}
	}
}
